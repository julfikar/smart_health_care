@extends('doctor.layout.doctor_layout')

@section('doctor_content')
    <div class="">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="text-capitalize mb-0 text-dark">
                    My Profile
                </h6>
            </div>
            <div class="col-md-6">
                <div class="float-md-right">
                    <ol class="breadcrumb mb-0 p-0 bg-transparent ">
                        <li class="breadcrumb-item"><a href="{{ url('/doctor/dashboard') }}" >Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="doctor-profile pt-4">
        <div class="card">
            <div class="card-header py-2">
                Basic Info
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('doctor.profile.update',$doctor->doctor->id) }}" class="register-validation" novalidate enctype="multipart/form-data">
                    @csrf
                    <div class="card border-0">
                        <div class="card-body ">

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <input type="text" name="title" value="{{ $doctor->doctor->title }}" class="form-control" id="inputPassword" placeholder="Enter Title">
                                </div>
                                <div class="invalid-feedback" role="alert">
                                    <strong>Please enter title</strong>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="number" name="phone" value="{{ $doctor->doctor->phone }}" class="form-control" id="inputPassword" placeholder="Enter Phone No">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="validationCustom04" class="col-sm-2 col-form-label">Department </label>
                                <div class="col-sm-10">
                                    <select class="custom-select form-control" id="validationCustom04" name="department_id" >
                                        <option  disabled value="">--Select One--</option>
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}" {{($doctor->doctor->department_id == $department->id)?'selected':''}}>{{ $department->name }}</option>
                                        @endforeach

                                    </select>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Fees</label>
                                <div class="col-sm-10">
                                    <input type="number" name="fees" value="{{ $doctor->doctor->fees }}" class="form-control" id="inputPassword" placeholder="Enter your fees">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Experience</label>
                                <div class="col-sm-10">
                                    <input type="number" name="year_of_experience" value="{{ $doctor->doctor->year_of_experience }}" class="form-control" id="year_of" placeholder="Year Of Experience">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Describe Yourself</label>
                                <div class="col-sm-10">
                                    <textarea class="joditeditor  form-control" name="describe_yourself" id="exampleFormControlTextarea1" rows="5" placeholder="Write your description...">
                                        {{ $doctor->doctor->describe_yourself }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Chamber Address</label>
                                <div class="col-sm-10">
                                    <textarea class="joditeditor form-control" name="chamber_information" id="exampleFormControlTextarea1" rows="5" placeholder="Your Chamber location...">
                                        {{ $doctor->doctor->chamber_information }}
                                    </textarea>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Photo</label>
                                <div class="col-sm-10">
                                    <div class="custom-file mb-3">
                                        <input type="file" name="photo" class="custom-file-input @error('photo') is-invalid @enderror" id="photo" >
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        @error('photo')
                                        <p class="alert alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-block btn-primary">Update Profile</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection