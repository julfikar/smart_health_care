@extends('doctor.layout.doctor_layout')

@section('doctor_content')

    <div class="">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="text-capitalize mb-0 text-dark">
                    My Patients
                </h6>
            </div>
            <div class="col-md-6">
                <div class="float-md-right">
                    <ol class="breadcrumb mb-0 p-0 bg-transparent ">
                        <li class="breadcrumb-item"><a href="{{ url('/doctor/dashboard') }}" >Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Patients</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="pt-4">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0">
                    <div class="card-header bg-info text-white">
                        My Patients
                    </div>
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-hover table-striped dataTable no-footer"
                               role="grid" aria-describedby="example1_info">
                            <thead>

                            <tr role="row">
                                <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 55.7813px;">
                                    Id
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 130.115px;">
                                    Appoinment ID
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 170.226px;">
                                    Patient Name
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 140.226px;" >
                                    Date
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 90.226px;" >
                                    Time
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                    style="width: 140.115px;" >
                                    Status
                                </th>
                                <th rowspan="1" colspan="1" class="sorting" tabindex="0" aria-controls="example1"
                                    style="width: 92.4479px;">
                                    Action
                                </th>
                            </tr>
                            </thead>

                            <tbody>

                            @foreach($mypatients as $appoinment)
                                <tr role="row" class="odd">
                                    <td class="sorting_1">{{ $loop->index+1 }}</td>
                                    <td><a href="{{ url('doctor/appoinments/'.$appoinment->id) }}">#SHC{{$appoinment->id}}</a></td>
                                    <td>{{ $appoinment->patient->user->name }}</td>
                                    <td>{{ $appoinment->date }}</td>
                                    <td>{{ $appoinment->time }}</td>
                                    <td>
                                        @if($appoinment->status == 0)
                                            {!! '<span class="badge p-2 font-weight-bold badge-warning">Pending</span>' !!}
                                        @elseif($appoinment->status == 1)
                                            {!! '<span class="badge p-2 font-weight-bold badge-success">Confirm</span>' !!}
                                        @elseif($appoinment->status == 2)
                                            {!! '<span class="badge p-2 font-weight-bold badge-danger">Cancel By Patient</span>' !!}
                                        @else
                                            {!! '<span class="badge p-2 font-weight-bold badge-danger">Cancel By Doctor</span>' !!}
                                        @endif
                                    </td>
                                    <td>
                                        <div class="btn-group mr-2" role="group" aria-label="First group">
                                            @if($appoinment->cancel_by_doctor == 1 )
                                                <a href="#" class="btn alert-danger btn-sm"
                                                   data-toggle="modal">Cancel Completed</a>
                                            @else
                                                <a href="#deleteModal{{$appoinment->id}}" class="btn alert-info btn-sm"
                                                   data-toggle="modal">Prescribe Now</a>
                                        @endif


                                        <!-- Delete Modal -->
                                            <div class="modal fade" id="deleteModal{{$appoinment->id}}" tabindex="-1" role="dialog"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document" style="max-width: 52rem !important;">
                                                    <div class="modal-content">
                                                        <div class="modal-header bg-info">
                                                            <h5 class="modal-title text-white" id="register">Add New Departments</h5>
                                                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body p-4">
                                                            <form method="POST"
                                                                  action="{{ route('doctor.prescription.store') }}" enctype="multipart/form-data">
                                                                @csrf
                                                                <input type="hidden" name="doctor_id" value="{{  $appoinment->doctor_id }}">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label for="name">{{ __('Name') }}</label>
                                                                            <input type="hidden" name="patient_id" value="{{  $appoinment->patient_id }}">
                                                                            <input id="name" type="text"
                                                                                   class="form-control"
                                                                                   value="{{  $appoinment->patient->user->name }}" required
                                                                                   autocomplete="name" autofocus disabled>

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12">
                                                                        <div class="form-group">
                                                                            <label for="password">{{ __('Prescription') }}</label>
                                                                            <div class="custom-file mb-3">
                                                                                <input type="file" name="prescription" class="custom-file-input " id="photo">
                                                                                <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>

                                                                <button type="submit" id="register"
                                                                        class="btn btn-info bg-one">{{ __('Send Prescription Now') }}</button>
                                                            </form>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
