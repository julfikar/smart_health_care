@extends('doctor.layout.doctor_layout')

@section('doctor_content')
    <div class="">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="text-capitalize mb-0 text-dark">
                    Appoinments
                </h6>
            </div>
            <div class="col-md-6">
                <div class="float-md-right">
                    <ol class="breadcrumb mb-0 p-0 bg-transparent ">
                        <li class="breadcrumb-item"><a href="{{ url('/doctor/dashboard') }}" >Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="doctor-profile pt-4">
        <div class="card">
            <div class="card-header py-2">
                Basic Info
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('doctor.appoinment.confirm',$appoinment->id) }}" class="register-validation" novalidate enctype="multipart/form-data">
                    @csrf

                    <div class="card border-0">
                        <div class="card-body ">

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Patient Name</label>
                                <div class="col-sm-10">
                                    <p class="form-control" >{{ $appoinment->patient->user->name }}</p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Date</label>
                                <div class="col-sm-10">
                                    <input type="date" name="date" value="{{ $appoinment->date }}" class="form-control" id="inputPassword" placeholder="Enter Phone No">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Time</label>
                                <div class="col-sm-10">
                                    <input type="text" name="time" value="{{ $appoinment->time }}" class="form-control" id="inputPassword" placeholder="Enter Phone No">
                                </div>
                            </div>

                           {{-- <div class="form-group row">
                                <label for="validationCustom04" class="col-sm-2 col-form-label">Department </label>
                                <div class="col-sm-10">
                                    <select class="custom-select form-control" id="validationCustom04" name="department_id" >
                                        <option  disabled value="">--Select One--</option>
                                        @foreach($departments as $department)
                                            <option value="{{ $department->id }}" {{($doctor->doctor->department_id == $department->id)?'selected':''}}>{{ $department->name }}</option>
                                        @endforeach

                                    </select>
                                </div>

                            </div>--}}

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Status</label>
                                <div class="col-sm-10">
                                   <p class="">
                                       @if($appoinment->status == 0)
                                           {!! '<span class="badge p-2 font-weight-bold badge-warning">Pending</span>' !!}
                                       @elseif($appoinment->status == 1)
                                           {!! '<span class="badge p-2 font-weight-bold badge-success">Confirm</span>' !!}
                                       @elseif($appoinment->status == 2)
                                           {!! '<span class="badge p-2 font-weight-bold badge-danger">Cancel By Patient</span>' !!}
                                       @else
                                           {!! '<span class="badge p-2 font-weight-bold badge-danger">Cancel By Doctor</span>' !!}
                                       @endif
                                   </p>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Patient Problem</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="describe_yourself" id="exampleFormControlTextarea1" rows="5" placeholder="Write your description...">
                                        {!! trim($appoinment->problem ) !!}
                                    </textarea>
                                </div>
                            </div>



                            <div class="form-group row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-block btn-primary">Confirm Appoinment</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection