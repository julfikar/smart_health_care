@extends('admin.layouts.admin_layout')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-user-md"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Doctors</span>
                        <span class="info-box-number">{{ $totalDoctor }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-stethoscope"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Appoinment</span>
                        <span class="info-box-number">{{ $totalAppoinment }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix hidden-md-up"></div>

            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i
                                    class="fas fa-procedures"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Total Patients</span>
                        <span class="info-box-number">{{ $totalPatient }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            <div class="col-12 col-sm-6 col-md-3">
                <div class="info-box mb-3" >
                        <span class="info-box-icon bg-fuchsia elevation-1" ><i
                                    class="fas fa-comment-medical "></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">All Feedbacks</span>
                        <span class="info-box-number">{{ $totalFeedbsck }}</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->

        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-12">

                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h4 class="card-title ">Newest Appoinments</h4>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="admin_datatable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Patient Name</th>
                                <th>Doctor Name</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th>Transaction ID</th>
                                <th>Payment</th>
                                <th style="width: 40px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($appoinments as $appoinment)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $appoinment->patient->user->name }}</td>
                                    <td>{{ $appoinment->doctor->user->name }}</td>
                                    <td> {{ $appoinment->date }}</td>
                                    <td> {!! $appoinment->status == 0? '<span class="badge p-2 font-weight-bold badge-warning">Pending</span>':'<span class="badge p-2 font-weight-bold badge-success">Confirm</span>' !!}</td>
                                    <td> {{ $appoinment->transactionid }}</td>
                                    <td> {!! $appoinment->is_paid == 0? '<span class="badge p-2 font-weight-bold badge-warning">Not Paid</span>':'<span class="badge p-2 font-weight-bold badge-success">Paid</span>' !!}</td>
                                    <td>
                                        <div class="btn-group mr-2" role="group" aria-label="First group">
                                            <span class="" data-toggle="tooltip" data-placement="top" title="Confirm Paid">
                                                <a href="#confirmpaid{{$appoinment->id}}"
                                                   class="btn btn-success btn-sm" data-toggle="modal" ><i class="fas fa-plus"></i>
                                                </a>
                                            </span>

                                            <span class="" data-toggle="tooltip" data-placement="top" title="Delete">
                                                <a href="#deleteModal{{$appoinment->id}}" class="text-white btn btn-danger btn-sm"
                                               data-toggle="modal" ><i class="fas fa-trash"></i></a>
                                            </span>

                                            <!-- Paid Modal -->
                                            <div class="modal fade" id="confirmpaid{{$appoinment->id}}" tabindex="-1"
                                                 role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <h4>Are You Sure to paid ?</h4>
                                                            <br>
                                                            <h3>Transaction ID : <span class="badge bg-success px-4">{{ $appoinment->transactionid  }}</span></h3>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form action="{{route('admin.confirm_paid', $appoinment->id)}}"
                                                                  method="post">
                                                                @csrf
                                                                <button type="submit" class="btn btn-danger">Ok
                                                                </button>
                                                            </form>
                                                            <button type="button" class="btn btn-warning"
                                                                    data-dismiss="modal">Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Cancel Modal -->
                                            <div class="modal fade" id="deleteModal{{$appoinment->id}}" tabindex="-1"
                                                 role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <h4>Are You Sure to Delete ?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form action="{{route('admin.department.destroy', $appoinment->id)}}"
                                                                  method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit" class="btn btn-danger">Ok
                                                                </button>
                                                            </form>
                                                            <button type="button" class="btn btn-warning"
                                                                    data-dismiss="modal">Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
@endsection
