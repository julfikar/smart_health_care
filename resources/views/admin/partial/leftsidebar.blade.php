<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-lightblue elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/admin/dashboard') }}" class="brand-link">
        <img src="{{ asset('public/frontend/img/apple-touch-icon.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
        <span class="brand-text font-weight-light">Smart Health Care</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-flat" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                {{--//menu-open--}}
                <li class="nav-item">
                    <a href="{{ url('/admin/dashboard/') }}" class="nav-link {{ (request()->is('admin/dashboard')) ?'active':'' }}">
                        <i class="nav-icon fas fa-briefcase-medical"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('admin.department.index') }}" class="nav-link {{ (request()->is('admin/department*')) ?'active':'' }}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            All Departments
                        </p>
                    </a>
                    {{--<ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin.department.index') }}" class="nav-link ">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Departments</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Add Departments</p>
                            </a>
                        </li>
                    </ul>--}}
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/doctor/all/') }}" class="nav-link {{ (request()->is('admin/doctor/*')) ?'active':'' }}">
                        <i class="nav-icon fas fa-pump-medical"></i>
                        <p>Manage Doctors</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('admin/appoinments/all') }}" class="nav-link {{ (request()->is('admin/appoinments/*')) ?'active':'' }}">
                        <i class="nav-icon fas fa-bed"></i>
                        <p>Manage Appoinments</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('admin/patient/all/') }}" class="nav-link {{ (request()->is('admin/patient/*')) ?'active':'' }}">
                        <i class="nav-icon fas fa-hand-holding-medical"></i>
                        <p>Manage Patients</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ url('admin/payments') }}" class="nav-link {{ (request()->is('admin/payments')) ?'active':'' }}">
                        <i class="nav-icon fas fa-money-check"></i>
                        <p>Payments Action</p>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>