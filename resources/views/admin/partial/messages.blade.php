

@if($message = Session::get('message'))
    <div class="alert fade show" id="success_message" style="color: #155724;background-color: #d4edda;border-color: #c3e6cb;">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif



@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if ($message = Session::get('success'))
    <div class="alert alert-success fade show text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($delete = Session::get('error'))
    <div class="alert alert-danger fade show text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $delete }}</strong>
    </div>
@endif
@if ($warning = Session::get('warning'))
    <div class="alert alert-warning fade show text-center">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $warning }}</strong>
    </div>
@endif