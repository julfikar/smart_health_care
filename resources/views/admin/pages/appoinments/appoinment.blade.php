@extends('admin.layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Appoinment Details</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Appoinment details</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">

                    <div class="card card-outline card-info">
                        <div class="card-header">
                            <h4 class="card-title ">All Appoinments</h4>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="admin_datatable" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Patient Name</th>
                                    <th>Doctor Name</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Transaction ID</th>
                                    <th>Payment</th>
                                    <th style="width: 40px">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($appoinments as $appoinment)
                                    <tr>
                                        <td>{{ $loop->index+1 }}</td>
                                        <td>{{ $appoinment->patient->user->name }}</td>
                                        <td>{{ $appoinment->doctor->user->name }}</td>
                                        <td> {{ $appoinment->date }}</td>
                                        <td>
                                            @if($appoinment->status == 0)
                                                {!! '<span class="badge p-2 font-weight-bold badge-warning">Pending</span>' !!}
                                            @elseif($appoinment->status == 1)
                                                {!! '<span class="badge p-2 font-weight-bold badge-success">Confirm</span>' !!}
                                            @elseif($appoinment->status == 2)
                                                {!! '<span class="badge p-2 font-weight-bold badge-danger">Cancel By Patient</span>' !!}
                                            @else
                                                {!! '<span class="badge p-2 font-weight-bold badge-danger">Cancel By Doctor</span>' !!}
                                            @endif
                                        </td>
                                        <td> {{ $appoinment->transactionid }}</td>
                                        <td> {!! $appoinment->is_paid == 0? '<span class="badge p-2 font-weight-bold badge-warning">Not Paid</span>':'<span class="badge p-2 font-weight-bold badge-success">Paid</span>' !!}</td>
                                        <td>
                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                <span class="" data-toggle="tooltip" data-placement="top" title="Confirm Paid">
                                                    <a href="#confirmpaid{{$appoinment->id}}"
                                                       class="btn btn-success btn-sm" data-toggle="modal" ><i class="fas fa-plus"></i>
                                                    </a>
                                                </span>

                                                <span class="" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <a href="#deleteModal{{$appoinment->id}}" class="text-white btn btn-danger btn-sm"
                                                       data-toggle="modal" ><i class="fas fa-trash"></i></a>
                                                </span>

                                                <!-- Paid Modal -->
                                                <div class="modal fade" id="confirmpaid{{$appoinment->id}}" tabindex="-1"
                                                     role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <h4>Are You Sure to paid ?</h4>
                                                                <br>
                                                                <h3>Transaction ID : <span class="badge bg-success px-4">{{ $appoinment->transactionid  }}</span></h3>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="{{route('admin.confirm_paid', $appoinment->id)}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <button type="submit" class="btn btn-danger">Ok
                                                                    </button>
                                                                </form>
                                                                <button type="button" class="btn btn-warning"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Cancel Modal -->
                                                <div class="modal fade" id="deleteModal{{$appoinment->id}}" tabindex="-1"
                                                     role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <h4>Are You Sure to Delete ?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="{{route('admin.appoinment.delete', $appoinment->id)}}"
                                                                      method="post">
                                                                    @csrf
                                                                    @method('delete')
                                                                    <button type="submit" class="btn btn-danger">Ok
                                                                    </button>
                                                                </form>
                                                                <button type="button" class="btn btn-warning"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection