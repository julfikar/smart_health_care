
@extends('admin.layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Doctor Details</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Doctor details</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="card card-outline">
                <h5 class="text-center card-header bg-light  text-success text-uppercase">Basic Info</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="">
                                <img src="{{ asset('public/doctors/avatar/'.$doctor->doctor->photo) }}" alt="doctor photo" class="img-fluid img-thumbnail" style="height: 250px; width: 100%;">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8 d-flex justify-content-end">
                            <div class="text-right w-75">
                                <h3  class="text-primary text-capitalize">{{ $doctor->name }}</h3>
                                <h6 class="text-danger">{{ $doctor->doctor->title }}</h6>
                                <h6 class="text-info">{{ $doctor->doctor->department->name }}</h6>
                                <table class="table w-100 ml-auto table-hover mt-2 mb-0 text-muted">
                                    <tbody>
                                    <tr>
                                        <td scope="row"  class="border-top-0 text-left">
                                            <span><i class="fas fa-envelope"></i> Email :</span>
                                        </td>
                                        <td class="border-top-0" > {{ $doctor->email }}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row" class="text-left">
                                            <span><i class="fas fa-phone fa-rotate-90"></i> Phone :</span>
                                        </td>
                                        <td> {{ $doctor->doctor->phone }}</td>
                                    </tr>
                                    <tr>
                                        <td scope="row" class="text-left">
                                            <span> <i class="fas fa-money-bill"></i> Fees :</span>
                                        </td>
                                        <td> {{ $doctor->doctor->fees }} Tk</td>
                                    </tr>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="doctors_info_editor">
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="card border-0">
                            <h5 class="text-center card-header bg-light text-success text-uppercase">Work & Education Info</h5>
                            <div class="card-body">
                                <div class=" text-muted">
                                    {!!  $doctor->doctor->describe_yourself !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="card border-0 ">
                            <h5 class="text-center card-header bg-light text-success text-uppercase">Chamber and Schedule</h5>
                            <div class="card-body">
                                <div class="text-muted">
                                    {!! $doctor->doctor->chamber_information  !!}
                                </div>
                            </div>
                            <div class="card-footer clearfix" style="display: block;">

                                <form action="{{ route('admin.doctor_confirm', $doctor->doctor->id) }}" method="post">
                                    @csrf
                                    @if($doctor->doctor->status)
                                        <input type="submit" class="float-right btn btn-success" value="Verified">
                                    @else
                                        <input type="submit" class="float-right btn btn-primary " value="Confirm Now">
                                    @endif

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection