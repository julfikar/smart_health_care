@extends('admin.layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard/') }}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.payments.index') }}">Payment</a></li>
                        <li class="breadcrumb-item active">Payment Edit</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card p-4">
                        <form method="POST" action="{{ route('admin.payments.update',$payment->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="name">{{ __('Payment Name') }}</label>
                                        <input id="name" type="text"
                                               class="form-control @error('name') is-invalid @enderror" name="name"
                                               value="{{ $payment->name }}" required autocomplete="name" autofocus placeholder="Enter name...">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="email">{{ __('Payment Account No.') }}</label>
                                        <input id="icon" type="text"
                                               class="form-control @error('icon') is-invalid @enderror" name="no"
                                               value="{{ $payment->no }}" required autocomplete="no" placeholder="Enter account number..." maxlength="12">
                                        @error('no')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="email">{{ __('Payment Account Type.') }}</label>

                                        <select name="type" id="type" class="form-control @error('type') is-invalid @enderror">
                                            <option value="">Select Account Type</option>
                                            <option value="agent" {{ $payment->type =='agent'?'selected':'' }}>Agent</option>
                                            <option value="personal" {{ $payment->type =='personal'?'selected':'' }}>Personal</option>
                                        </select>
                                        @error('type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>


                                    <div class="form-group">
                                        <label for="customFile">{{ __('Payment Image') }}</label>

                                        <div class="custom-file">
                                            <input type="file" name="payment_image" class="custom-file-input" id="customFile">
                                            <label class="custom-file-label" for="customFile"></label>
                                        </div>
                                        @error('payment_image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        {{--<label for="">{{ __('Old Image') }} : </label>--}}
                                        <img src="{{ asset('public/payments/'.$payment->image) }}" alt="" class="img-fluid" style="width: 200px; height: 100px">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" id="register" class="btn btn-info bg-one float-right">{{ __('Update Payment Method') }}</button>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
