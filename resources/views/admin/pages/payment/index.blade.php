@extends('admin.layouts.admin_layout')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Payments</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ url('/admin/dashboard/') }}">Dashboard</a></li>
                    <li class="breadcrumb-item active">Payments</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card card-outline card-info">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="">All Payments Method</h4>
                            </div>
                            <div class="col-6">
                                <a href="" class="btn btn-primary  float-right" data-toggle="modal" data-target="#register"> <i class="fas fa-plus-circle" aria-hidden="true"></i>
                                    Add New Payment
                                </a>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="admin_datatable" class="table table-bordered table-striped " >
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Account No</th>
                                <th style="width: 40px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td class="align-middle text-center">{{ $loop->index+1 }}</td>
                                    <td class="align-middle text-center">{{ $payment->name }}</td>
                                    <td class="align-middle text-center"> <img src="{{ asset('public/payments/'.$payment->image) }}" alt="" style="width: 200px;height: 100px"></td>
                                    <td class="align-middle text-center"> {{ $payment->no }}</td>
                                    <td class="align-middle text-center">
                                        <div class="btn-group mr-2" role="group" aria-label="First group">
                                            <a href="{{ route('admin.payments.edit', $payment->id ) }}"
                                               class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit or View"><i class="fas fa-edit"></i></a>

                                            <span class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <a href="#deleteModal{{$payment->id}}" class="text-white"
                                               data-toggle="modal" ><i class="fas fa-trash"></i></a>
                                             </span>
                                            <!-- Delete Modal -->
                                            <div class="modal fade" id="deleteModal{{$payment->id}}" tabindex="-1"
                                                 role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <h4>Are You Sure to Delete ?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form action="{{route('admin.payments.destroy', $payment->id)}}"
                                                                  method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit" class="btn btn-danger">Ok
                                                                </button>
                                                            </form>
                                                            <button type="button" class="btn btn-warning"
                                                                    data-dismiss="modal">Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->


<!--For add new payment -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog " role="document" >
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title text-white" id="register">Add New Payments</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <form method="POST" action="{{ route('admin.payments.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">{{ __('Payment Name') }}</label>
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter name...">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('Payment Account No.') }}</label>
                                <input id="icon" type="text"
                                       class="form-control @error('icon') is-invalid @enderror" name="no"
                                       value="{{ old('no') }}" required autocomplete="no" placeholder="Enter account number..." maxlength="12">
                                @error('no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="email">{{ __('Payment Account Type.') }}</label>

                                <select name="type" id="type" class="form-control @error('icon') is-invalid @enderror">
                                    <option value="">Select Account Type</option>
                                    <option value="agent">Agent</option>
                                    <option value="personal">Personal</option>
                                </select>
                                @error('no')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="customFile">{{ __('Payment Image') }}</label>

                                <div class="custom-file">
                                    <input type="file" name="payment_image" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile"></label>
                                </div>
                                @error('payment_image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <button type="submit" id="register" class="btn btn-info bg-one">{{ __('Add Payment') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
