@extends('admin.layouts.admin_layout')

@section('content')

<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Departments</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">Departments</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="card card-outline card-info">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-6">
                                <h4 class="">All Departments </h4>
                            </div>
                            <div class="col-6">
                                <a href="" class="btn btn-primary  float-right" data-toggle="modal" data-target="#register"> <i class="fas fa-plus-circle" aria-hidden="true"></i>
                                    Add New Department</a>
                            </div>
                        </div>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="admin_datatable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Icon</th>
                                <th>Motto</th>
                                <th style="width: 40px">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departments as $department)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $department->name }}</td>
                                    <td class="text-info fa-2x">{!! $department->icon !!}</td>
                                    <td> {{ $department->motto }}</td>
                                    <td>
                                        <div class="btn-group mr-2" role="group" aria-label="First group">
                                            <a href="{{ route('admin.department.edit', $department->id ) }}"
                                               class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="top" title="Edit or View"><i class="fas fa-edit"></i></a>
                                            <span class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                            <a href="#deleteModal{{$department->id}}" class="text-white"
                                               data-toggle="modal" ><i class="fas fa-trash"></i></a>
                                        </span>
                                            <!-- Delete Modal -->
                                            <div class="modal fade" id="deleteModal{{$department->id}}" tabindex="-1"
                                                 role="dialog" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <h4>Are You Sure to Delete ?</h4>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <form action="{{route('admin.department.destroy', $department->id)}}"
                                                                  method="post">
                                                                @csrf
                                                                @method('delete')
                                                                <button type="submit" class="btn btn-danger">Ok
                                                                </button>
                                                            </form>
                                                            <button type="button" class="btn btn-warning"
                                                                    data-dismiss="modal">Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content -->


<!--For add new department -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog " role="document" style="max-width: 52rem !important;">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title text-white" id="register">Add New Departments</h5>
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-4">
                <form method="POST" action="{{ route('admin.department.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">{{ __('Name') }}</label>
                                <input id="name" type="text"
                                       class="form-control @error('name') is-invalid @enderror" name="name"
                                       value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter name...">
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email">{{ __('Department Icon') }}</label>
                                <input id="icon" type="text"
                                       class="form-control @error('icon') is-invalid @enderror" name="icon"
                                       value="{{ old('icon') }}" required autocomplete="icon" placeholder="Enter icon...">
                                @error('icon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="password">{{ __('Motto') }}</label>
                                <input id="password" type="text"
                                       class="form-control @error('motto') is-invalid @enderror" name="motto"
                                       required autocomplete="motto" placeholder="Enter motto...">
                                @error('motto')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="description">{{ __('Description') }}</label>
                                <textarea class="form-control textarea" name="description" id="description" cols="30" rows="15" placeholder="Department Description ...">

                                </textarea>
                            </div>

                        </div>
                    </div>

                    <button type="submit" id="register" class="btn btn-info bg-one">{{ __('Add Department') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
