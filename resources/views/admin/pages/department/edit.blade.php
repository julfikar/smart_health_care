@extends('admin.layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card p-4">
                        <form method="POST" action="{{ route('admin.department.update',$department->id) }}">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">{{ __('Name') }}</label>
                                        <input id="name" type="text"
                                               class="form-control @error('name') is-invalid @enderror" name="name"
                                               value="{{ $department->name }}" required autocomplete="name" autofocus placeholder="Enter name...">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">{{ __('Department Icon') }}</label>
                                        <input id="icon" type="text"
                                               class="form-control @error('icon') is-invalid @enderror" name="icon"
                                               value="{{ $department->icon }}" required autocomplete="icon" placeholder="Enter icon...">
                                        @error('icon')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="password">{{ __('Motto') }}</label>
                                        <input id="password" type="text"
                                               class="form-control @error('motto') is-invalid @enderror" value="{{ $department->motto }}" name="motto"
                                               required autocomplete="motto" placeholder="Enter motto...">
                                        @error('motto')
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label for="description">{{ __('Description') }}</label>
                                        <textarea class="form-control textarea" name="description" id="description" cols="30" rows="15" placeholder="Department Description ...">
                                            {{ $department->description }}
                                        </textarea>
                                    </div>

                                </div>
                            </div>

                            <button type="submit" id="register" class="btn btn-info bg-one">{{ __('Update Department') }}</button>
                        </form>
                    </div>
                </div>

            </div>
            <!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection
