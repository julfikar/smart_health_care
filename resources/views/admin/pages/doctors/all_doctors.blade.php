@extends('admin.layouts.admin_layout')

@section('content')

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Doctor Details</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Doctor details</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">


            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-outline  card-primary">

                              <h4 class="card-title ">All Doctors</h4>
                              <div class="card-tools">
                                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                      <i class="fas fa-minus"></i>
                                  </button>
                                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                                      <i class="fas fa-times"></i>
                                  </button>
                              </div>

                        </div>
                        <!-- /.card-header -->
                    <div class="card-body " style="display: block;">
                            <div class="table-responsive">
                                <table id="admin_datatable" class="table table-striped border m-0">
                                    <thead>
                                    <tr>
                                        <th>Doctor ID</th>
                                        <th>Name</th>
                                        <th style="width:100px;">Status</th>
                                        <th class="text-center" style="width:100px;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($doctors as $doctor)
                                        <tr>
                                            <td><a href="{{ url('admin/doctor/details/'.$doctor->user->id) }}">SMCD122{{ $loop->index+1 }}</a></td>
                                            <td>{{ $doctor->user->name }} </td>
                                            <td>
                                                {!!  $doctor->status == 0? '<span class="badge p-2 font-weight-bold badge-warning">Pending</span>':'<span class="badge p-2 font-weight-bold badge-success">Confirm</span>' !!}
                                            </td>

                                            <td class="text-center">
                                                <div class="btn-group mr-2" role="group" aria-label="First group">
                                                    <a href="{{ url('admin/doctor/details/'.$doctor->user->id) }}"
                                                       class="btn btn-primary btn-sm"><i class="fas fa-eye"></i></a>
                                                    <a href="#deleteModal{{$doctor->id}}" class="btn btn-danger btn-sm"
                                                       data-toggle="modal"><i class="fas fa-trash"></i></a>

                                                    <!-- Delete Modal -->
                                                    <div class="modal fade" id="deleteModal{{$doctor->id}}" tabindex="-1"
                                                         role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-body">
                                                                    <h4>Are You Sure to Delete ?</h4>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <form action="" method="post">
                                                                        {{ csrf_field() }}
                                                                        <button type="button" class="btn btn-danger">Ok
                                                                        </button>
                                                                    </form>
                                                                    <button type="button" class="btn btn-warning"
                                                                            data-dismiss="modal">Cancel
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix" style="display: block;">

                            <a href="javascript:void(0)" class="btn btn-sm btn-primary float-right">View All Doctors</a>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection