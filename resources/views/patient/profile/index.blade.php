@extends('patient.layout.patient_layout')

@section('patient_content')

    <div class="">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="text-capitalize mb-0 text-dark">
                    Dashboard
                </h6>
            </div>
            <div class="col-md-6">
                <div class="float-md-right">
                    <ol class="breadcrumb mb-0 p-0 bg-transparent ">
                        <li class="breadcrumb-item"><a href="#" >Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="doctor-profile pt-4">
        <div class="card">
            <div class="card-header py-2">
                Basic Info
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('patient.profile.update',$patient->patient->id) }}" class="register-validation" novalidate enctype="multipart/form-data">
                    @csrf
                    <div class="card border-0">
                        <div class="card-body ">

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Father name</label>
                                <div class="col-sm-10">
                                    <input type="text" name="father_name" value="{{ $patient->patient->father_name }}" class="form-control" id="inputPassword" placeholder="Enter father name">
                                </div>
                                <div class="invalid-feedback" role="alert">
                                    <strong>Please enter father name</strong>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Mobile No</label>
                                <div class="col-sm-10">
                                    <input type="text" name="mobile_no" value="{{ $patient->patient->mobile_no }}" class="form-control" id="inputPassword" placeholder="Enter Phone No">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Age</label>
                                <div class="col-sm-10">
                                    <input type="number" name="age" value="{{ $patient->patient->age }}" class="form-control" id="inputPassword" placeholder="Enter Phone No">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Occupation</label>
                                <div class="col-sm-10">
                                    <input type="text" name="occupation" value="{{ $patient->patient->occupation }}" class="form-control" id="inputPassword" placeholder="Enter occupation">
                                </div>
                                <div class="invalid-feedback" role="alert">
                                    <strong>Please enter occupation</strong>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Date Of Birth</label>
                                <div class="col-sm-10">
                                    <input type="date" name="date_of_birth" value="{{ $patient->patient->date_of_birth }}" class="form-control" id="inputPassword" placeholder="Enter date of birth">
                                </div>
                                <div class="invalid-feedback" role="alert">
                                    <strong>Please enter occupation</strong>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Gender</label>
                                <div class="col-sm-10">
                                    <div class="form-control border-0">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="gender" value="male" {{($patient->patient->gender == 'male')?'checked':''}}>Male
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="gender" value="female" {{($patient->patient->gender == 'female')?'checked':''}}>Female
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="gender" value="others" {{($patient->patient->gender == 'others')?'checked':''}}>Others
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="validationCustom04" class="col-sm-2 col-form-label">Religion </label>
                                <div class="col-sm-10">
                                    <select class="custom-select form-control" id="validationCustom04" name="religion" >
                                        <option value="">--Select One--</option>
                                        <option value="islam" {{($patient->patient->religion == 'islam')?'selected':''}}>Islam</option>
                                        <option value="christan" {{($patient->patient->religion == 'christan')?'selected':''}}>Christan</option>
                                        <option value="hindu" {{($patient->patient->religion == 'hindu')?'selected':''}}>Hindu</option>
                                        <option value="bodhist" {{($patient->patient->religion == 'bodhist')?'selected':''}}>Bodhist</option>
                                        <option value="others" {{($patient->patient->religion == 'others')?'selected':''}}>others</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="validationCustom04" class="col-sm-2 col-form-label">Blood Group </label>
                                <div class="col-sm-10">
                                    <select class="custom-select form-control" id="validationCustom04" name="blood_group" >
                                        <option  value="">--Select One--</option>
                                        <option value="A+" {{($patient->patient->blood_group == 'A+')?'selected':''}}>A+</option>
                                        <option value="A-" {{($patient->patient->blood_group == 'A-')?'selected':''}}>A-</option>
                                        <option value="B+" {{($patient->patient->blood_group == 'B+')?'selected':''}}>B+</option>
                                        <option value="B-" {{($patient->patient->blood_group == 'B-')?'selected':''}}>B-</option>
                                        <option value="AB+" {{($patient->patient->blood_group == 'AB+')?'selected':''}}>AB+</option>
                                        <option value="AB-" {{($patient->patient->blood_group == 'AB-')?'selected':''}}>AB-</option>
                                        <option value="O+" {{($patient->patient->blood_group == 'O+')?'selected':''}}>O+</option>
                                        <option value="O-" {{($patient->patient->blood_group == 'O+')?'selected':''}}>O-</option>

                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Maritial status</label>
                                <div class="col-sm-10">
                                    <div class="form-control border-0">
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="maritial_status" value="married" {{($patient->patient->maritial_status == 'married')?'checked':''}}>Married
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="maritial_status" value="unmarried" {{($patient->patient->maritial_status == 'unmarried')?'checked':''}}>Unmarried
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                <textarea class="joditeditor form-control" name="address" id="exampleFormControlTextarea1" rows="5" placeholder="Your Address...">
                                    {{ $patient->patient->address }}
                                </textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPassword" class="col-sm-2 col-form-label">Photo</label>
                                <div class="col-sm-10">
                                    <div class="custom-file mb-3">
                                        <input type="file" name="avatar" class="custom-file-input @error('photo') is-invalid @enderror" id="photo" >
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        @error('avatar')
                                        <p class="alert alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-block btn-primary">Update Profile</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection