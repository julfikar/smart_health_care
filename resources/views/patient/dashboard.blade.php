@extends('patient.layout.patient_layout')

@section('patient_content')
    <div class="">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="text-capitalize mb-0 text-dark">
                    Dashboard
                </h6>
            </div>
            <div class="col-md-6">
                <div class="float-md-right">
                    <ol class="breadcrumb mb-0 p-0 bg-transparent ">
                        <li class="breadcrumb-item"><a href="#" >Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="user_dashboard_widget py-4">
        <div class="row">
            <div class="col-md-4">
                <a href="{{ route('patient.profile') }}" class="text-dark ">
                    <div class="card border-0">
                        <div class="card-body text-center">
                            <span class="text-info"><i class="fas fa-user fa-2x  "></i></span>
                            <h4 class="pt-3">My Profile</h4>
                            <p class="">Update Profile</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4">
                <div class="card border-0">
                    <div class="card-body text-center">
                        <span class="text-success"><i class="fas fa-notes-medical fa-2x"></i></span>
                        <h4 class="pt-3">{{ $my_total_appoinments }}</h4>
                        <p class="">My Appoinment</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">

                    <div class="card border-0">
                        <div class="card-body text-center">
                            <span class="text-warning"><i class="fas fa-comment-medical fa-2x"></i></span>
                            <h4 class="pt-3">{{ $my_total_feedbacks }}</h4>
                            <p class="">All Feedbacks</p>
                        </div>
                    </div>

            </div>
        </div>

        <div class="mt-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card border-0">
                        <div class="card-header bg-info text-white">
                            All Appoinments
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover table-striped dataTable no-footer"
                                   role="grid" aria-describedby="example1_info">
                                <thead>

                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 55.7813px;">
                                        Id
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 130.115px;">
                                        Appoinment ID
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 170.226px;">
                                        Doctor Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 140.226px;" >
                                        Date
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 90.226px;" >
                                        Time
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 90.226px;" >
                                        Paid
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 140.115px;" >
                                        Status
                                    </th>
                                    <th rowspan="1" colspan="1" class="sorting" tabindex="0" aria-controls="example1"
                                        style="width: 92.4479px;">
                                        Action
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($appoinments as $appoinment)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $loop->index+1 }}</td>
                                        <td><a href="{{ url('doctor/appoinments/'.$appoinment->id) }}">#SHC{{$appoinment->id}}</a></td>
                                        <td>{{ $appoinment->doctor->user->name }}</td>
                                        <td>{{ $appoinment->date }}</td>
                                        <td>{{ $appoinment->time }}</td>
                                        <td>{!! $appoinment->is_paid == 0 ?'<span class="badge badge-warning">Pending</span>':'<span class="badge badge-success">Paid</span>'  !!}</td>
                                        <td>{!! $appoinment->status== 0 ?'<span class="badge badge-warning">Pending</span>':'<span class="badge badge-success">Confirmed</span>'  !!}</td>
                                        <td>
                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                <a href="#deleteModal2" class="btn btn-danger btn-sm"
                                                   data-toggle="modal">Cancel</a>

                                                <!-- Delete Modal -->
                                                <div class="modal fade" id="deleteModal2" tabindex="-1" role="dialog"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-body">
                                                                <h4>Are You Sure to Cancel this appoinment ?</h4>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <form action="http://localhost/click_to_buy/public/myadmin/orders/delete/2"
                                                                      method="post">
                                                                    <input type="hidden" name="_token"
                                                                           value="UbZFPcblhXRDj7yBk22DtSok0WczPWm0FW5PVQMg">
                                                                    <button type="submit" class="btn btn-danger">Ok
                                                                    </button>
                                                                </form>
                                                                <button type="button" class="btn btn-warning"
                                                                        data-dismiss="modal">Not Cancel
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
