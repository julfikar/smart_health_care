@extends('frontend.layout.master')


@section('content')
    <!--===== Start User Dashboard Section  =====-->
    <section class="py-5 bg_magic" id="user_dashboard_section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 ">
                    <div class="card border-0 " style="height: 200px;">
                            <div class="card-body">
                                <div class="w-100 mb-3">
                                    <img src="{{ asset('public/patients/avatar/'.$patient->patient->avatar) }}" class="d-block mx-auto img-fluid img-thumbnail rounded-circle" alt="Profile Picture" style="width: 100px; height: 100px;">
                                </div>
                                <h6 class="text-center">{{ $patient->name }} <span class="text-success"><i class="fas fa-check-circle"></i></span></h6>
                            </div>
                        </div>
                    <div class="card border-0 user_dashboard_menu_part">

                        <h6 class="text-muted mb-0">Menu</h6>
                        <div class="dropdown-divider"></div>
                        <div class="">
                            <div class="user_dashboard_menu py-3">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="{{url('patient/dashboard')}}" class=" {{ request()->is('patient/dashboard')?'active':'' }}">
                                            <i class="fas fa-tachometer-alt"></i>
                                            <span class=""> Dashboard</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('patient/profile')}}" class=" {{ request()->is('patient/profile')?'active':'' }}">
                                            <i class="fas fa-cog"></i>
                                            <span class=""> Edit Profile</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{url('patient/prescription')}}" class=" {{ request()->is('patient/prescription')?'active':'' }}">
                                            <i class="fas fa-prescription"></i>
                                            <span class=""> My Prescription</span>
                                        </a>
                                    </li>


                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    @include('frontend.partial.messages')
                    @yield('patient_content')
                </div>

            </div>
        </div>
    </section>
    <!--===== End User Dashboard Section  =====-->
@endsection
