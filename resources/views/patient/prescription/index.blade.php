@extends('patient.layout.patient_layout')

@section('patient_content')
    <div class="">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6 class="text-capitalize mb-0 text-dark">
                    Dashboard
                </h6>
            </div>
            <div class="col-md-6">
                <div class="float-md-right">
                    <ol class="breadcrumb mb-0 p-0 bg-transparent ">
                        <li class="breadcrumb-item"><a href="#" >Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="user_dashboard_widget py-4">

        <div class="mt-4">
            <div class="row">
                <div class="col-md-12">
                    <div class="card border-0">
                        <div class="card-header bg-info text-white">
                            My Prescription
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover table-striped dataTable no-footer"
                                   role="grid" aria-describedby="example1_info">
                                <thead>

                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 55.7813px;">
                                        Id
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 170.226px;">
                                        Doctor Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 140.226px;" >
                                        Date
                                    </th>

                                    <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1"
                                        style="width: 140.115px;" >
                                        Prescription
                                    </th>
                                    <th rowspan="1" colspan="1" class="sorting" tabindex="0" aria-controls="example1"
                                        style="width: 92.4479px;">
                                        Action
                                    </th>
                                </tr>
                                </thead>

                                <tbody>

                                @foreach($my_prescriptions as $my_prescription)
                                    <tr role="row" class="odd">
                                        <td class="sorting_1">{{ $loop->index+1 }}</td>
                                        <td>{{ $my_prescription->doctor->user->name }}</td>
                                        <td>{{ $my_prescription->created_at }}</td>

                                        <td>
                                            <img src="{{ asset('public/prescriptions/'.$my_prescription->prescription) }}" alt="" width="150px" height="100px">
                                        </td>

                                        <td>
                                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                                <a href="#deleteModal{{ $loop->index+1 }}" class="btn btn-info btn-sm"
                                                   data-toggle="modal">View</a>

                                                <!-- Delete Modal -->
                                                <div class="modal fade" id="deleteModal{{ $loop->index+1 }}" tabindex="-1" role="dialog"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document" style="max-width: 800px !important;">
                                                        <div class="modal-content">
                                                            <div class="modal-header alert-info">
                                                                <h3 class="">My Prescription </h3>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="{{ asset('public/prescriptions/'.$my_prescription->prescription) }}" alt="" class="img-fluid" width="800px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
