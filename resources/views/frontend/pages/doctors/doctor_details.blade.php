@extends('frontend.layout.master')

@section('content')

    <!--===== Start Doctoras details Section  =====-->

    <section class="bg_magic py-5" id="doctors_datails_setion">
        <div class="container-fluid container-md ">
            <div class="row">
                <div class="col-12">
                    @include('frontend.partial.messages')
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 pl-md-0 pl-lg-3">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12 col-md-3 col-lg-4">
                                    <div class="">
                                        <img src="{{ asset('public/doctors/avatar/'.$doctor->doctor->photo) }}" alt="doctor photo" class="img-fluid img-thumbnail" style="height: 250px; width: 100%;">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-9 col-lg-8 d-flex justify-content-end">
                                    <div class="text-right w-100">
                                        <h3  class="text-primary text-capitalize">{{ $doctor->name }}</h3>
                                        <h6 class="text-danger">{{ $doctor->doctor->title }}</h6>
                                        <h6 class="text-info">{{ $doctor->doctor->department->name }}</h6>
                                       <div class="w-75 ml-0 ml-sm-auto">
                                           <table class="table table-hover mt-2 mb-0 text-muted">
                                               <tbody>
                                               <tr>
                                                   <td scope="row"  class="border-top-0 text-left">
                                                       <span><i class="fas fa-envelope"></i> Email :</span>
                                                   </td>
                                                   <td class="border-top-0" > {{ $doctor->email }}</td>
                                               </tr>
                                               <tr>
                                                   <td scope="row" class="text-left">
                                                       <span><i class="fas fa-phone fa-rotate-90"></i> Phone :</span>
                                                   </td>
                                                   <td> {{ $doctor->doctor->phone }}</td>
                                               </tr>
                                               <tr>
                                                   <td scope="row" class="text-left">
                                                       <span> <i class="fas fa-money-bill"></i> Fees :</span>
                                                   </td>
                                                   <td> {{ $doctor->doctor->fees }} Tk</td>
                                               </tr>

                                               </tbody>
                                           </table>
                                       </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="doctors_info_editor">
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="card border-0">
                                    <div class="card-body">
                                        <h5 class="text-center section_title text-success">Doctor Info</h5>
                                        <hr>
                                        <div class=" text-muted">
                                            {!!  $doctor->doctor->describe_yourself !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <div class="card border-0 ">
                                    <div class="card-body">
                                        <h5 class="text-center section_title text-success">Chamber and Schedule</h5>
                                        <div class="text-muted">
                                            <hr>
                                            {!! $doctor->doctor->chamber_information  !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @if(Auth::user() && Auth::user()->role_id ==3)
                    <div class="col-md-3 px-md-0 px-lg-3">
                        <div class="card border-0 mt-3 mt-md-0">
                            <div class="card-body">
                                <h5 class="text-center section_title mb-3">Book Appoinment</h5>
                                <hr>
                                <form action="{{ route('appoinment.store') }}" method="post" class="appoinment_validation" novalidate>
                                    @csrf
                                    <input type="hidden" name="doctor_id" value="{{ $doctor->doctor->id  }}">
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id  }}">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name<sup class="text-danger">*</sup> </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{ Auth::user()->name }}" aria-describedby="emailHelp" placeholder="Patient name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Patient Phone<sup class="text-danger">*</sup> </label>
                                        <input type="text" class="form-control" value="{{ Auth::user()->patient->mobile_no }}" id="exampleInputEmail2" placeholder="Patient mobile" minlength="11" required>
                                        <div class="invalid-feedback">
                                            Please provide a valid phone Number.
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Gender<sup class="text-danger">*</sup> </label>
                                        <div class="">
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="gender" value="male" {{(Auth::user()->patient->gender == 'male')?'checked':''}}>Male
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="gender" value="female" {{(Auth::user()->patient->gender == 'female')?'checked':''}}>Female
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <label class="form-check-label">
                                                    <input type="radio" class="form-check-input" name="gender" value="others" {{(Auth::user()->patient->gender == 'others')?'checked':''}}>Others
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Patient Age<sup class="text-danger">*</sup> </label>
                                        <input type="number" class="form-control" value="{{ Auth::user()->patient->age }}" id="exampleInputEmail3" placeholder="Age" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Appoinment Date<sup class="text-danger">*</sup> </label>
                                        <input type="date" class="form-control" name="appoinment_date" id="exampleInputEmail4" placeholder="dd-mm-yy" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="time">Your Time</label>
                                        <select class="custom-select form-control" id="validationCustom04" name="appoinment_time" required>
                                            <option value="">--Select One--</option>
                                            <option value="8:00 am">8:00 am</option>
                                            <option value="8:30 am">8:30 am</option>
                                            <option value="9:30 am">9:30 am</option>
                                            <option value="4:00 pm">4:00 pm</option>
                                            <option value="4:30 pm">4:30 pm</option>
                                            <option value="8:00 pm">8:00 pm</option>
                                            <option value="8:30 pm">8:30 pm</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Problem<sup class="text-danger">*</sup> </label>
                                        <textarea class="form-control" name="problem" id="exampleFormControlTextarea1" rows="3" placeholder="Describe your problem in short" minlength="10" required></textarea>
                                        <div class="invalid-feedback">
                                            Please write something
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="time">Payment Now</label>
                                        <select id="payments" class="custom-select form-control" name="payment_id" required>
                                            <option value="" disabled selected>--Select One--</option>
                                            <option value="bkash">Bkash Payment</option>
                                            <option value="rocket">Rocket Payment</option>

                                        </select>
                                    </div>

                                    <div class="form-group d-none" id="payment_bkash">
                                        <p class="alert-success p-2 text-justify">
                                            Please send  <span class="badge badge-info"> {{ $doctor->doctor->fees }}</span> Taka to this Bkash no and write your Transaction ID below there.
                                        </p>
                                        <h4 class="alert-info p-2 text-center">01245-785747</h4>
                                    </div>

                                    <div class="form-group d-none" id="payment_rocket">
                                        <p class="alert-secondary p-2 text-justify">
                                            Please send <span class="badge badge-info"> {{ $doctor->doctor->fees }}</span> Taka to this Rocket no and write your Transaction ID below there.
                                        </p>
                                        <h4 class="alert-info p-2 text-center">01245-785747-3</h4>

                                    </div>

                                    <div class="form-group d-none" id="transaction_id">
                                        <input type="text" name="transactionid" class="form-control" id="exampleInputEmail1" value="" aria-describedby="emailHelp" placeholder="Payment Transaction ID" required>
                                    </div>

                                    <button class="btn btn-block btn-primary" type="submit">Book Now</button>

                                </form>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-3 px-md-0 px-lg-3">
                        <div class="card border-0 mt-3 mt-md-0">
                            <div class="card-body">
                                <h5 class="text-center section_title mb-3 ">Book Appoinment</h5>
                                <hr>
                                <form action="" class="appoinment_validation" novalidate>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name<sup class="text-danger">*</sup> </label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Patient name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Patient Phone<sup class="text-danger">*</sup> </label>
                                        <input type="text" class="form-control" id="exampleInputEmail2" placeholder="Patient mobile" minlength="11" required>
                                        <div class="invalid-feedback">
                                            Please provide a valid phone Number.
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Gender<sup class="text-danger">*</sup> </label>
                                        <div class="">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" id="inlineCheckbox1" name="gender"  value="Male"  required>
                                                <label class="form-check-label" for="inlineCheckbox1">Male</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" id="inlineCheckbox2" name="gender" value="option2" required>
                                                <label class="form-check-label" for="inlineCheckbox2">Female</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" id="inlineCheckbox3" name="gender" value="option3" required>
                                                <label class="form-check-label" for="inlineCheckbox3">Others</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail3">Patient Age<sup class="text-danger">*</sup> </label>
                                        <input type="number" class="form-control" id="exampleInputEmail3" placeholder="Age" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail2">Appoinment Date<sup class="text-danger">*</sup> </label>
                                        <input type="date" class="form-control" id="exampleInputEmail4" placeholder="dd-mm-yy" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="time">Your Time</label>
                                        <select class="custom-select form-control" id="validationCustom04" name="appoinment_time" disabled>
                                            <option value="">--Select One--</option>
                                            <option value="8:00 am">8:00 am</option>
                                            <option value="8:30 am">8:30 am</option>
                                            <option value="9:30 am">9:30 am</option>
                                            <option value="4:00 pm">4:00 pm</option>
                                            <option value="4:30 pm">4:30 pm</option>
                                            <option value="8:00 pm">8:00 pm</option>
                                            <option value="8:30 pm">8:30 pm</option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Problem<sup class="text-danger">*</sup> </label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Describe your problem in short" minlength="10" required disabled></textarea>
                                        <div class="invalid-feedback">
                                            Please write something
                                        </div>
                                    </div>

                                    <a href="{{ route('login') }}" class="btn btn-block alert-warning" type="button">Please Login For Booking</a>

                                </form>
                            </div>
                        </div>
                    </div>
                @endif

            </div>

            <!--===== Start Doctoras details Review Section  =====-->
            <div class="row mt-3">
                <div class="col-12">

                    <div class="card">
                        <div class="card-body">
                            <ul class="nav nav-tabs nav-pills mx-auto" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">All Feedbacks</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Give a Review</a>
                                </li>

                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="card-body ">

                                        @if($feedbacks->count() > 0)
                                            @foreach($feedbacks as $feedback)
                                                <div class="media mb-3 ">
                                                    <img src=" {{ asset('public/patients/avatar/'.$feedback->user->patient->avatar) }}" class="mr-3 rounded" alt="..." style="height: 60px;width: 60px;">
                                                    <div class="media-body">
                                                        <h5 class="mt-0 text-capitalize">{{ $feedback->user->name }}</h5>
                                                        {{ $feedback->description }}
                                                    </div>
                                                </div>
                                                <hr>
                                            @endforeach
                                        @else
                                            <h4 class="alert alert-info">No review yet. </h4>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="card-body">

                                        @if( !Auth::user())
                                            <h5 class="alert alert-danger">Please login to review.</h5>
                                        @else
                                            <form action="{{ route('feedback.store') }}" class="w-100" method="post">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                                <input type="hidden" name="doctor_id" value="{{ $doctor->id }}">
                                                <div class="form-group">
                                                    <label for="exampleFormControlTextarea1" class="sr-only">Problem<sup class="text-danger">*</sup> </label>
                                                    <textarea class="form-control" name="description"  id="exampleFormControlTextarea1" rows="3" placeholder="Write whats your mind say." minlength="4" required>
                                                      {{  (Auth::user()->role_id === '3') ? ((Auth::user()->feedback->doctor_id == $doctor->id) ? Auth::user()->feedback->description : "") : '' }}

                                                    </textarea>
                                                    <div class="invalid-feedback">
                                                        Please write something
                                                    </div>
                                                </div>
                                                <div class="d-flex justify-content-end">
                                                    <button class="btn btn-primary w-50" type="submit">Give Review Now</button>
                                                </div>

                                            </form>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--===== End Doctoras details Review Section  =====-->
        </div>
    </section>

    <!--===== End Doctoras details Section  =====-->






@endsection