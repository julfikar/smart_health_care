@extends('frontend.layout.master')

@section('content')

    <!-- ======= Start Contact Section ======= -->
    <section id="contact" class="bg_magic contact py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="magic-fancy-title ">
                        <h2>Search Results </h2>
                        <span><small></small><i class="fa fa-stethoscope"></i></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-12">
                    @foreach($doctors as $doctor)
                        <div class="all_single_doctors mb-3">
                            <div class="card border-0">
                                <div class="card-body">
                                    <div class="media">
                                        <img src="{{ asset('public/doctors/avatar/'.$doctor->photo) }}" class="mr-3 rounded-circle" alt="..." style="width: 100px; height: 100px;">
                                        <div class="media-body">
                                            <div class="row ">
                                                <div class="col-lg-8 col-md-7 col-sm-12 border-right ">
                                                    <a href="{{ url('/doctor_details/'.$doctor->user->slug) }}" class="text-dark">
                                                        <h5 class="font-weight-bold text-uppercase">{{ $doctor->user->name }}</h5>
                                                    </a>
                                                    <h6 class="text-danger">{{ $doctor->department->name }}</h6>
                                                    <p class="">{{ $doctor->title }}</p>
                                                </div>
                                                <div class="col-lg-4 col-md-5 col-sm-12 all_single_doctors_right">
                                                    <p class=""><i class="fas fa-check"></i> {{ $doctor->year_of_experience }} </p>
                                                    <p class=""><i class="fas fa-check"></i> Fees: {{ $doctor->fees }}Tk </p>
                                                    <br>
                                                    <a href="{{ url('/doctor_details/'.$doctor->user->slug) }}" class="btn btn-outline-primary btn-sm">Book Appoinment</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="col-lg-3 col-md-12">

                   {{-- <div class="card border-0 p-2">
                        <div class="">
                            <h5 class="section_title mt-3"> Departments</h5>

                            <div class="all_single_doctors_dept">
                                <ul class="list-unstyled">
                                    @foreach($departments as $department)
                                        <li class="dropdown-item py-2">
                                            <a href="{{ url('/department_details/'.$department->slug) }}" class="text-muted">{!! $department->icon !!} {{ $department->name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>
                    </div>--}}
                </div>
            </div>
        </div>
    </section>
    <!--===== End Contact Section  =====-->
@endsection