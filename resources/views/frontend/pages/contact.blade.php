@extends('frontend.layout.master')

@section('content')
    <!-- ======= Start Contact Section ======= -->
    <section id="contact" class=" contact py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="magic-fancy-title ">
                        <h2>Contact Us</h2>
                        <span><small></small><i class="fa fa-stethoscope"></i></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row mt-3">
                <div class="col-lg-6">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="info-box">
                                <i class="fas fa-map-marker-alt"></i>
                                <h3>Our Address</h3>
                                <p>A122 , Islamic university, kushtia</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box mt-4">
                                <i class="fas fa-envelope"></i>
                                <h3>Email Us</h3>
                                <p>info@smarthealth.com<br>contact@smarthealth.com</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="info-box mt-4">
                                <i class="fas fa-phone fa-rotate-90"></i>
                                <h3>Call Us</h3>
                                <p>+01753 987696<br>+01587 598874</p>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6">
                    <form action="{{ route('contact_us.store') }}" method="post" role="form" class="php-email-form">
                        @csrf
                        <div class="form-row">
                            <div class="col form-group">
                                @if(!Auth::user())
                                     <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                @else
                                    <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" id="name" placeholder="{{ Auth::user()->name }}" data-rule="email" data-msg="Please enter a valid email" required/>
                                @endif
                            </div>
                            <div class="col form-group">
                                @if(!Auth::user())
                                    <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" required/>
                                @else
                                    <input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" id="email" placeholder="{{ Auth::user()->email }}" data-rule="email" data-msg="Please enter a valid email" required/>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                        </div>

                        <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!--===== End Contact Section  =====-->
@endsection