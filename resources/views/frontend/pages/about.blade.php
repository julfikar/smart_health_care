@extends('frontend.layout.master')

@section('content')
    <!-- ======= Start Contact Section ======= -->
    <section id="contact" class="bg_magic contact py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="magic-fancy-title">
                        <h2>About Us</h2>
                        <span><small></small><i class="fa fa-stethoscope"></i></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="container">
            <div class="row">
                <div class="col-md-6 text-muted">
                    <div class="card border-0">
                        <div class="card-body  text-justify">
                            Each and everyone is suffering from some or the other sickness.
                            With increase in the technology more application are being used through the mobile devices.
                            Sometimes there will be situations where the doctors will not be available in the hospitals.
                            What if there is an application that will allow the user to consult the doctor through the online mode?
                            <br>
                            Yes, It is possible by the smart health consulting system.This application simplify the task of patient and doctor.
                        </div>
                    </div>
                </div>
                <div class="col-md-6 mt-sm-3 mt-md-0">
                    <div class="">
                        <img src="{{ asset('public/frontend/img/smart_bg.png') }}" alt="" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--===== End Contact Section  =====-->
@endsection