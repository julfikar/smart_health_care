@extends('frontend.layout.master')

@section('content')

<!--===== Start Departmnt Details Section  =====-->
<section class="bg_magic" id="department_details_section">
    <div class="section_overlay" style="height: 150px; background: linear-gradient( rgba(0,0,0,0.5), rgba(0,0,0,0.2)), url({{ asset('public/frontend/img/bannerbg.jpg') }}) no-repeat 50% 50%/cover;">
        <div class="container h-100">
            <div class="row h-100 d-flex align-items-center">
                <div class="col-md-12 text-center  text-white">
                    <h1 class="">{{ $department->name }}</h1>
                    <p class="text-warning">{{ $department->motto }}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card border-0 shadow-sm">
                    <div class="card-body">
                        <div class="dept-icon d-flex justify-content-center">
                            {!! $department->icon !!}
                        </div>
                        <div class="mt-4">
                            {!! $department->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--===== End Department details Section  =====-->

@endsection