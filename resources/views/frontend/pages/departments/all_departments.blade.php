@extends('frontend.layout.master')

@section('content')

<!--====== End Department Section  =======-->
<section id="departments_section" class="bg-white py-5">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="magic-fancy-title">
                <h2>Our Departrments</h2>
                <span><small></small><i class="fa fa-stethoscope"></i></span>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($departments as $department)
            <div class="col-md-4 col-lg-3">
                <a href="{{ url('/department_details/'.$department->slug) }}" class="text-dark">
                    <div class="single-department">
                        {!! $department->icon !!}
                        <h3>{{ $department->name }}</h3>
                        <p>{{ $department->motto }}</p>
                    </div>
                </a>
            </div>
        @endforeach
    </div>


</div>
</section>
<!--====== End department Section  =======-->

@endsection