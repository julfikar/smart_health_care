@extends('frontend.layout.master')

@section('content')
    <!--====== Start hero banner Section  =======-->
    <section class="hero_banar_section">
        <div class="overlay"></div>
        <div class="hero_banar_container">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="d-flex justify-content-center ">
                            <div class=" w-75 text-white">
                                <h1 class="text-center">Find your Doctor!</h1>
                                <p class="text-center ">Get Telemedicine and Doctor Video Consultation service with thousands
                                    of Doctors in Bangladesh.</p>
                                <div class="mt-4">
                                    <form action="{{ route('search') }}" method="GET">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="sr-only">Search doctors</label>
                                            <input type="text" name="search_name" class="form-control magic_input" id="exampleInputEmail1"
                                                   aria-describedby="emailHelp" placeholder="Doctor Name, Phone, Fees">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1" class="sr-only">Example select</label>
                                            <select name="search_department" class="form-control magic_input" id="exampleFormControlSelect1">
                                                <option value="0" selected>--Select Department (optional)--</option>
                                                @foreach($departments as $department)
                                                <option value="{{ $department->id }}">{{ $department->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <button type="submit" class="btn btn-primary btn-block  mt-2">Appoinment Now</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== End hero banner Section  =======-->


    <!--====== End Department Section  =======-->
    <section id="departments_section" class="bg-white py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="magic-fancy-title">
                        <h2>Our Departrments</h2>
                        <span><small></small><i class="fa fa-stethoscope"></i></span>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($departments as $department)
                <div class="col-md-4 col-lg-3">
                    <a href="{{ url('/department_details/'.$department->slug) }}" class="text-dark">
                        <div class="single-department">
                            {!! $department->icon !!}
                            <h3>{{ $department->name }}</h3>
                            <p>{{ str_limit($department->motto,$limit=50,$end='...') }}</p>
                        </div>
                    </a>
                </div>
              @endforeach
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="{{ url('/all_departments') }}" class="btn btn-info px-5 mt-4">
                        View All Departments <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </div>

        </div>
    </section>
    <!--====== End department Section  =======-->

    <!--====== Start our Doctors Section  =======-->
    <section class="py-5" id="dedicated_doctors_section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="magic-fancy-title">
                        <h2>OUR DEDICATED DOCTORS</h2>
                        <span><small></small><i class="fa fa-stethoscope"></i></span>
                    </div>
                </div>
            </div>
            <div class="row">

                @foreach($doctors as $doctor)
                <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                    <a href="{{ url('/doctor_details/'.$doctor->slug) }}" class="w-100">
                        <div class="member aos-init aos-animate" data-aos="fade-up" data-aos-delay="100" style="min-height: 370px">
                            <div class="member-img">
                                <img src="{{ asset('public/doctors/avatar/'.$doctor->doctor->photo) }}" class="img-fluid" alt="doctor image" style="height: 240px;width: 100%;">
                            </div>
                            <div class="member-info">
                                <h6 class="">{{ str_limit($doctor->name ,$limit=20,$end='...') }}</h6>
                                <span class="text-muted"> {{ str_limit($doctor->doctor->title ,$limit=50,$end='...') }} </span>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach

                    {{-- @gmail.com  /--}}
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a href="{{ url('/all_doctors') }}" class="btn btn-info px-5 mt-4">
                        View All doctors <i class="fas fa-arrow-right"></i>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--====== End our Doctors Section  =======-->

@endsection