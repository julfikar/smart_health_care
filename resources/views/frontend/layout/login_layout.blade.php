<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/frontend/img/apple-touch-icon.png') }}">

    <title>{{ config('app.name', 'Smart Health Care system') }}</title>
{{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
      rel="stylesheet">--}}

<!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/fonts/css/all.min.css') }}">
    <!-- Jodit editor -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/plugin/jodit.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/plugin/sweetalert2.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/style.css') }}">

</head>
<body>


@include('frontend.partial.messages')

<main class="">
    @yield('content')
</main>


<script type="text/javascript" src="{{  asset('public/frontend/js/jquery-3.5.1.slim.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/main.js') }}"></script>
<!-- Jodit editor -->
<script type="text/javascript" src="{{ asset('public/frontend/plugin/jodit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/plugin/bs-custom-file-input.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/plugin/sweetalert2.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        bsCustomFileInput.init();

        $(".joditeditor").each(function (el) {
            var $this = $(this);
            var buttons = $this.data("buttons");
            buttons = !buttons
                ? "bold,underline,italic,hr,|,ul,ol,|,align,paragraph,|,image,table,link,undo,redo"
                : buttons;

            var editor = new Jodit(this, {
                uploader: {
                    insertImageAsBase64URI: true,
                },
                toolbarAdaptive: false,
                defaultMode: "1",
                toolbarSticky: false,
                showXPathInStatusbar: false,
                buttons: buttons,
            });
        });
    });


</script>

</body>
</html>
