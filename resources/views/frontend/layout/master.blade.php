<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/frontend/img/apple-touch-icon.png') }}">

    <title>{{ config('app.name', 'Smart Health Care system') }}</title>
    {{--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet">--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/fonts/css/all.min.css') }}">
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/plugin/datatables.css') }}">
    <!-- Jodit editor -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/plugin/jodit.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/plugin/sweetalert2.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/style.css') }}">

</head>
<body>

   <!--===== Start Header Section  =====-->
   <header class="" id="header_section">
       <!--<div class="header_top">
           <div class="container">
               <div class="row">
                   <div class="col-md-12 py-1">
                       <div class="d-flex align-items-center justify-content-between">
                           <div class="">
                               <i class="fas fa-phone-alt"></i>
                               Call Us Now +8801993 954442
                           </div>
                           <div class="">
                               <a href="#" class="text-secondary">
                                   Registration now
                               </a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>-->
       <div class="header_bottom ">
           <nav class="navbar navbar-expand-lg navbar-light bg-white py-2">
               <div class="container-fluid px-5">
                   <a class="navbar-brand" href="{{ url('/') }}">
                       <img src="{{ asset('public/frontend/img/logo.png') }}" alt="" class="img-fluid" style="width: 280px;height: 40px;">
                   </a>
                   <button class="navbar-toggler" type="button" data-toggle="collapse"
                           data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                           aria-expanded="false" aria-label="Toggle navigation">
                       <span class="navbar-toggler-icon"></span>
                   </button>

                   <div class="collapse navbar-collapse" id="navbarSupportedContent">
                       <ul class="navbar-nav ml-auto mr-4 main-menu">
                           <li class="nav-item {{ (request()->is('/'))?'active':'' }}">
                               <a class="nav-link" href="{{ url('/') }}">Home</a>
                           </li>
                           <li class="nav-item {{ (request()->is('about'))?'active':'' }}">
                               <a class="nav-link" href="{{ url('/about') }}">About</a>
                           </li>
                           <li class="nav-item {{ (request()->is('all_doctors'))?'active':'' }}">
                               <a class="nav-link " href="{{ url('/all_doctors') }}">Doctors</a>
                           </li>
                           <li class="nav-item {{ (request()->is('all_departments'))?'active':'' }}">
                               <a class="nav-link" href="{{ url('/all_departments') }}">Departments</a>
                           </li>
                           <li class="nav-item {{ (request()->is('contact'))?'active':'' }}">
                               <a class="nav-link " href="{{ url('/contact') }}">Contact</a>
                           </li>

                       </ul>

                       <hr class="d-lg-none">
                       <ul class="navbar-nav navbar-right">

                           <!-- Authentication Links -->
                           @guest
                               @if (Route::has('login'))

                                   @auth
                                       <li class="nav-item ">
                                           <a href="{{ url('/') }}" class=" btn btn-primary text-white">
                                               Home <i class="fas fa-arrow-circle-right"></i>
                                           </a>
                                       </li>
                                   @else
                                       <li class="nav-item ">
                                           <a href="{{ route('login') }}" class=" btn btn-primary text-white">
                                               Login <i class="fas fa-arrow-circle-right"></i>
                                           </a>
                                       </li>

                                   @endauth

                               @endif
                           @else
                               <li class="nav-item dropdown">
                                   <a id="navbarDropdown" class="btn btn-info dropdown-toggle text-capitalize " href="javascript:void(0)" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                      <i class="fas fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                   </a>

                                   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                       <a class="dropdown-item" href="{{ (Auth::user()->role->id == 2)? route('doctor.dashboard'): route('patient.dashboard') }}">
                                           {{ __('Dashboard') }}
                                       </a>

                                       <a class="dropdown-item" href="{{ route('logout') }}"
                                          onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                           {{ __('Logout') }}
                                       </a>

                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                       </form>
                                   </div>
                               </li>
                           @endguest


                           {{--<li class="nav-item ">
                               <a href="login.html" class=" btn btn-primary text-white">
                                   Login <i class="fas fa-arrow-circle-right"></i>
                               </a>
                               <!--<a class="dropdown-toggle btn btn-primary text-white" id="navbarDropdown3" role="button"
                                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                   Login
                               </a>
                               <div class="dropdown-menu" aria-labelledby="navbarDropdown3">
                                   <a class="dropdown-item" href="#">Action</a>
                                   <a class="dropdown-item" href="#">Another action</a>
                                   <div class="dropdown-divider"></div>
                                   <a class="dropdown-item" href="#">Something else here</a>
                               </div>-->
                           </li>--}}
                       </ul>
                   </div>
               </div>
           </nav>
       </div>

   </header>
   <!-- ======== End Header Section  ======== -->

    <main class="">
        @yield('content')
    </main>



   <!--====== Start Footer Bottom Section  =======-->
   <footer id="footer">
       <div class="footer-top">
           <div class="container">
               <div class="row">

                   <div class="col-lg-4 col-md-6">
                       <div class="footer-info">
                           <h3>Smart Health Care</h3>
                           <p>
                               Islamic University <br>
                               Kushtia-Jinaydah <br><br>
                               <strong>Phone:</strong> +88 01966 5454 45<br>
                               <strong>Email:</strong> info@smarthealth.com<br>
                           </p>
                           <div class="social-links mt-3">
                               <a href="#" class="twitter"><i class="fab fa-twitter"></i></a>
                               <a href="#" class="facebook"><i class="fab fa-facebook"></i></a>
                               <a href="#" class="google-plus"><i class="fab fa-skype"></i></a>
                               <a href="#" class="linkedin"><i class="fab fa-linkedin"></i></a>
                           </div>
                       </div>
                   </div>

                   <div class="col-lg-4 col-md-6 footer-links">
                       <h4>Useful Links</h4>
                       <ul>
                           <li><i class="fas fa-angle-right"></i> <a href="{{ url('/') }}">Home</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="{{ url('/about') }}">About us</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="{{ url('/all_doctors') }}">All Doctors</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="{{ url('/all_departments') }}">All Departments</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="#">FAQ</a></li>
                       </ul>
                   </div>

                   {{--<div class="col-lg-3 col-md-6 footer-links">
                       <h4>Our Services</h4>
                       <ul>
                           <li><i class="fas fa-angle-right"></i> <a href="#"> Web Design</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="#"> Web Development</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="#"> Product Management</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="#">Marketing</a></li>
                           <li><i class="fas fa-angle-right"></i> <a href="#">Graphic Design</a></li>
                       </ul>
                   </div>--}}

                   <div class="col-lg-4 col-md-6 footer-newsletter">
                       <h4>Our Newsletter</h4>
                       <p>To get our news in time. </p>
                       <form action="" method="post">
                           <input type="email" name="email"><input type="submit" value="Subscribe">
                       </form>

                   </div>

               </div>
           </div>
       </div>

       <div class="container">
           <div class="copyright">
               &copy; Copyright <strong><span>Julfikar Ali</span></strong>. All Rights Reserved
           </div>
       </div>
   </footer>
   <!--====== End Footer Bottom Section  =======-->


<script type="text/javascript" src="{{  asset('public/frontend/js/jquery-3.5.1.slim.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/js/popper.min.js') }}"></script>
   <!-- Datatables -->
<script type="text/javascript" src="{{ asset('public/frontend/plugin/datatables.js') }}"></script>
   <!-- Jodit editor -->
<script type="text/javascript" src="{{ asset('public/frontend/plugin/jodit.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/plugin/bs-custom-file-input.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/frontend/plugin/sweetalert2.min.js') }}"></script>
   <script type="text/javascript" src="{{ asset('public/frontend/js/main.js') }}"></script>

   <script type="text/javascript">
       $(document).ready(function () {

           // for remove space from textarea
           $('textarea').each(function(){
                   $(this).val($(this).val().trim());
               }
           );

           bsCustomFileInput.init();

           $(".joditeditor").each(function (el) {
               var $this = $(this);
               var buttons = $this.data("buttons");
               buttons = !buttons
                   ? "bold,underline,italic,hr,|,ul,ol,|,align,paragraph,|,image,table,link,undo,redo"
                   : buttons;

               var editor = new Jodit(this, {
                   uploader: {
                       insertImageAsBase64URI: true,
                   },
                   toolbarAdaptive: false,
                   defaultMode: "1",
                   toolbarSticky: false,
                   showXPathInStatusbar: false,
                   buttons: buttons,
               });
           });

           $('#example1').DataTable();



           $('#payments').change(function () {
               $payment_method = $('#payments').val();

              if($payment_method == 'bkash'){
                  $('#payment_bkash').removeClass('d-none');
                  $('#payment_rocket').addClass('d-none');
                  $('#transaction_id').removeClass('d-none');
              }else if ($payment_method== 'rocket'){
                  $('#payment_rocket').removeClass('d-none');
                  $('#payment_bkash').addClass('d-none');
                  $('#transaction_id').removeClass('d-none');
              }else{
                  $('#payment_bkash').addClass('d-none');
                  $('#payment_rocket').addClass('d-none');
              }

           });

       });


   </script>

</body>
</html>
