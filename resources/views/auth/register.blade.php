@extends('frontend.layout.login_layout')

@section('content')
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>--}}

<!--===== Start Register Section  =====-->
<section class="bg_magic py-3" id="registration_section">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-7">
                <div class="text-center mb-2">
                    <a href="{{ url('/') }}" class="">
                        <img src="{{ asset('public/frontend/img/logo.png') }}" alt="" class="img-fluid" style="width: 280px; height: 50px;">
                    </a>
                </div>

                <form method="POST" action="{{ route('register') }}"  class="register-validation" novalidate>
                        @csrf
                    <div class="card border-0">
                        <div class="card-body p-4">
                            <h4 class="text-center">SIGN UP</h4>
                            <hr>
                            <div class="form-group mt-4">
                                <label for="name" class="">Full Name <sup class="text-danger">*</sup></label>
                                <input id="name" type="text" class="form-control magic_input @error('name') is-invalid @enderror" name="name" placeholder="Full name here" value="{{ old('name') }}" required autocomplete="name" >
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="form-group mt-3">
                                <label for="formGroupExampleInput" class="">Email <sup class="text-danger">*</sup></label>
                                <input id="email" type="email" class="form-control magic_input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" placeholder="Email " required autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror

                            </div>
                            <div class="form-group mt-3">
                                <label for="formGroupExampleInput2" class="">Password <sup class="text-danger">*</sup></label>
                                <input id="password" type="password" class="form-control magic_input @error('password') is-invalid @enderror" name="password" placeholder="Password " required autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="from-control mb-3">
                                <label for="validationCustom04">User Type  <sup class="text-danger">*</sup></label>
                                <select class="custom-select magic_input" id="validationCustom04" name="role_id" required>
                                    <option selected disabled value="">--Select One--</option>
                                    <option value="3">Patient</option>
                                    <option value="2">Doctor</option>
                                </select>
                                @error('role_id')
                                 <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-block btn-primary ">SIGN UP NOW</button>

                            <div class="text-center mt-3">
                                Do you have an account? <a href="{{ route('login') }}" class="text-danger">Login</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--===== End Loigin Section  =====-->

@endsection
