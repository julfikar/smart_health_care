@extends('frontend.layout.login_layout')

@section('content')

<!--===== Start Loigin Section  =====-->
<section class="bg_magic py-4" id="login_section">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-sm-12 col-md-7 ">
                <div class="text-center mb-2">
                    <a href="{{ url('/') }}" class="">
                        <img src="{{ asset('public/frontend/img/logo.png') }}" alt="" class="img-fluid" style="width: 280px; height: 50px;">
                    </a>
                </div>
                <form method="POST" action="{{ route('login') }}" class="register-validation" novalidate>
                    @csrf
                    <div class="card border-0">
                        <div class="card-body p-5">
                            <h4 class="text-center">LOGIN</h4>
                            <hr>
                            <div class="form-grou mt-4">
                                <label for="email" class="col-md-4 col-form-label text-md-right sr-only">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="magic_input form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror

                            </div>

                            <div class="form-group mt-3">
                                <label for="formGroupExampleInput2" class="sr-only">Password <sup class="text-danger">*</sup></label>
                                <input id="password" type="password" class="magic_input form-control @error('password') is-invalid @enderror" name="password" placeholder="Password " required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <button class="btn btn-block btn-primary">LOGIN</button>

                            <div class="text-center mt-3">
                                Don't have an account? <a href="{{ route('register') }}" class="text-danger">Sign UP</a>
                            </div>
                            <div class="text-center mt-3">
                                @if (Route::has('password.request'))
                                    <a class="text-danger" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>


<!--===== End Loigin Section  =====-->


@endsection
