<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//    Frontend page route
Route::get('/', 'HomepageController@index')->name('homepage');
Route::get('/all_doctors', 'HomepageController@all_doctors');
Route::get('/doctor_details/{slug}', 'HomepageController@doctor_details');

Route::get('/all_departments', 'HomepageController@all_departments');
Route::get('/department_details/{slug}', 'HomepageController@department_details');

Route::get('/contact','HomepageController@contact');
Route::resource('contact_us','ContactUsController');
Route::get('/about','HomepageController@about');
Route::get('/search','HomepageController@search')->name('search');

Route::resource('feedback','FeedbackController');

Route::resource('appoinment','AppoinmentController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//    For Admin
Route::group([ 'as'=> 'admin.','prefix'=> 'admin', 'namespace'=> 'Admin', 'middleware'=>['auth','admin']], function (){

    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::resource('/department','DepartmentController');
    Route::resource('/payments','PaymentController');

    Route::get('/doctor/details/{id}','DashboardController@doctor_details');
    Route::post('/doctor_confirm/{id}','DashboardController@doctor_confirm')->name('doctor_confirm');
    Route::get('/doctor/all','DashboardController@all_doctors');

    Route::post('/appoinment_confirm_paid/{id}','DashboardController@confirm_paid')->name('confirm_paid');
    Route::get('/appoinments/all','DashboardController@all_appoinments');
    Route::delete('/appoinments/delete/{id}','DashboardController@delete_appoinments')->name('appoinment.delete');

    Route::get('/patient/all','PatientsController@index');
    Route::get('/patient/details/{id}','PatientsController@patient_details');

});

//    For Doctor
Route::group([ 'as'=> 'doctor.','prefix'=> 'doctor', 'namespace'=> 'Doctor', 'middleware'=>['auth','doctor']], function (){

    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::get('/profile','DashboardController@profile')->name('profile');
    Route::post('/profile/update/{id}','DashboardController@profile_update')->name('profile.update');
    Route::get('/appoinments/{id}','DashboardController@appoinment_details');
    Route::get('/mypatients','DashboardController@mypatients');
    Route::post('/appoinment/update/{id}','DashboardController@cancel_appoinment')->name('appoinment.update');
    Route::post('/appoinment/confirm/{id}','DashboardController@confirm_appoinment')->name('appoinment.confirm');

    Route::post('/prescriptions/','DashboardController@send_prescription')->name('prescription.store');

});

//    For Patient
Route::group([ 'as'=> 'patient.','prefix'=> 'patient', 'namespace'=> 'Patient', 'middleware'=>['auth','patient']], function (){

    Route::get('/dashboard','DashboardController@index')->name('dashboard');
    Route::get('/profile','DashboardController@profile')->name('profile');
    Route::get('/prescription','DashboardController@my_prescription')->name('prescription');
    Route::post('/profile/update/{id}','DashboardController@profile_update')->name('profile.update');

});