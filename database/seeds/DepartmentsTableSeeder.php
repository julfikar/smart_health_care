<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name'=>'General physician',
            'slug'=>'general-physician',
            'icon'=>'<i class="fa fa-user-secret"></i>',
            'motto'=>'Find the right family in your neighborhood',
            'description'=>'Find the right family in your neighborhood',
        ]);

        DB::table('departments')->insert([
            'name'=>'Dentist',
            'slug'=>'dentist',
            'icon'=>'<i class="fa fa-teeth"></i>',
            'motto'=>'Teething troubles? schedule a dental checkup',
            'description'=>'Teething troubles? schedule a dental checkup',
        ]);
        DB::table('departments')->insert([
            'name'=>'Nutrition',
            'slug'=>'nutrition',
            'icon'=>'<i class="fas fa-neuter"></i>',
            'motto'=>'Get guidence on eating right, weighit management and sports and nutrition',
            'description'=>'Get guidence on eating right, weighit management and sports and nutrition',
        ]);
        DB::table('departments')->insert([
            'name'=>'Orthopdist',
            'slug'=>'orthopdist',
            'icon'=>'<i class="fa fa-thermometer-quarter"></i>',
            'motto'=>'For bone and joints issues, spinal injurijes and more',
            'description'=>'Get guidence on eating right, weighit management and sports and nutrition',
        ]);

        DB::table('departments')->insert([
            'name'=>'Gastrology',
            'slug'=>'gastrology',
            'icon'=>'<i class="fas fa-lightbulb"></i>',
            'motto'=>'Explore for issues related to digestive system, liver and pancreas',
            'description'=>'Explore for issues related to digestive system, liver and pancreas',
        ]);
        DB::table('departments')->insert([
            'name'=>'Neurology',
            'slug'=>'neurology',
            'icon'=>'<i class="fas fa-laptop"></i>',
            'motto'=>'Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas.',
            'description'=>'Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas.',
        ]);
        DB::table('departments')->insert([
            'name'=>'Eye',
            'slug'=>'eye',
            'icon'=>'<i class="fas fa-eye"></i>',
            'motto'=>'The eye is the lamp of the body.',
            'description'=>'The eye is the lamp of the body.',
        ]);

        DB::table('departments')->insert([
            'name'=>'Skin',
            'slug'=>'skin',
            'icon'=>'<i class="fab fa-skyatlas"></i>',
            'motto'=>'Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas.',
            'description'=>'Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas.',
        ]);

    }
}
