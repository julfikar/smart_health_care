<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = [
        'title', 'user_id','phone', 'department_id','year_of_experience','fees','photo',
        'year_of_experience','chamber_information'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function department(){
        return $this->belongsTo('App\Model\Department');
    }

}
