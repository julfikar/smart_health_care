<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedbacks';
    protected $fillable = [
        'user_id', 'doctor_id','description'
    ];


    public function user(){
        return $this->belongsTo('App\User');
    }


}
