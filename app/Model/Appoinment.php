<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Appoinment extends Model
{
    public function patient(){
        return $this->belongsTo('App\Model\Patient','patient_id','id');
    }

    public function doctor(){
        return $this->belongsTo('App\Model\Doctor');
    }
}
