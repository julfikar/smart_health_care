<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{

    protected $fillable = [
         'user_id', 'father_name','mobile_no','date_of_birth','gender','occupation',
        'maritial_status','religion','blood_group','avatar','address'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }


}
