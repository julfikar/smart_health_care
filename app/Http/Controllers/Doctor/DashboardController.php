<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Model\Appoinment;
use App\Model\Department;
use App\Model\Doctor;
use App\Model\Feedback;
use App\Model\Prescription;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function index(){
        $id = Auth::user()->id;
        $doctor = User::where('id',$id)->first();

        $doctor_id = $doctor->doctor->id;
        $totalAppoinment = Appoinment::Where('doctor_id',$doctor_id)->get()->count();
        $totalFeedback = Feedback::Where('doctor_id',$doctor_id)->get()->count();

        $appoinments = Appoinment::where('doctor_id',$doctor_id)->where('status', '!=',1)->orderBy('id','asc')->get();

        return view('doctor.dashboard', compact('doctor','appoinments','totalAppoinment','totalFeedback'));
    }

    public function profile(){
        $id = Auth::user()->id;
        $doctor = User::where('id',$id)->first();
        $departments = Department::orderBy('name','asc')->get();
        return view('doctor.profile.index', compact('doctor','departments'));
    }


    public function profile_update(Request $request,$id){


        $doctor = Doctor::where('id',$id)->get()->first();

        if($request->photo != ''){
            $this->validate($request,[
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
                [
                'photo.image'=>'Provide a valid image.',
                ]
            );

            $path = public_path().'/doctors/avatar/';

            //code for remove old file
            if($doctor->photo != ''  && $doctor->photo != null){
                $file_old = $path.$doctor->photo;
                unlink($file_old);
            }

            //upload new file
            $image = $request->photo;
            $imagename = time().'.'.$image->extension();
            $image->move($path, $imagename);

            //for update in table
//            $doctor->update(['photo' => $imagename]);

//            $doctor['photo'] = $request->imagename;
            $doctor->update(['photo' => $imagename]);
        }

        $doctor['title'] = $request->title;
        $doctor['department_id'] = $request->department_id;
        $doctor['phone'] = $request->phone;
        $doctor['year_of_experience'] = $request->year_of_experience;
        $doctor['fees'] = $request->fees;
        $doctor['describe_yourself'] = $request->describe_yourself;
        $doctor['chamber_information'] = $request->chamber_information;

        $doctor->update();
        return redirect()->back()->with('message', 'User Updated Successfully.');

    }

    public function appoinment_details($id){
        $did = Auth::user()->id;
        $doctor = User::where('id',$did)->first();
        $appoinment = Appoinment::where('id',$id)->orderBy('id','asc')->first();
        return view('doctor.appoinments.index', compact('doctor','appoinment'));
    }



    public function cancel_appoinment($id){

        $appoinment = Appoinment::find($id);

        $appoinment->cancel_by_doctor = 1;
        $appoinment->status = 3;

        $appoinment->save();

        Session()->flash('message', 'Appoinment has been cancel successfully');
        return redirect()->back();
    }

    public function confirm_appoinment($id){
        $appoinment = Appoinment::find($id);

        $appoinment->status = 1;

        $appoinment->save();

        Session()->flash('message', 'Appoinment has been confirm successfully');
        return redirect()->back();
    }

    public function mypatients(){
        $id = Auth::user()->id;
        $doctor = User::where('id',$id)->first();

        $doctor_id = $doctor->doctor->id;

        $mypatients = Appoinment::where('doctor_id',$doctor_id)->where('status', '=', 1)->orderBy('id','asc')->get();

        return view('doctor.mypatients.all_patients', compact('doctor','mypatients'));

    }


    public function send_prescription(Request $request){

        $prescription = new Prescription();

        if($request->prescription != ''){
            $this->validate($request,[
                'prescription' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
                [
                    'prescription.image'=>'Provide a valid image.',
                ]
            );

            $path = public_path().'/prescriptions/';

            //upload new file
            $image = $request->prescription;
            $imagename = time().'.'.$image->extension();
            $image->move($path, $imagename);

            $prescription['prescription'] = $imagename;
        }

        $prescription['doctor_id'] = $request->doctor_id;
        $prescription['patient_id'] = $request->patient_id;

        $prescription->save();
        return redirect()->back()->with('message', 'Prescription send Successfully.');

    }



}
