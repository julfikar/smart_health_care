<?php

namespace App\Http\Controllers\Patient;

use App\Http\Controllers\Controller;
use App\Model\Appoinment;
use App\Model\Feedback;
use App\Model\Patient;
use App\Model\Prescription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function index(){
        $id = Auth::user()->id;
        $patient = User::where('id',$id)->first();
        $patient_id = $patient->patient->id;

        $my_total_appoinments = Appoinment::where('patient_id',$patient_id)->count();
        $my_total_feedbacks = Feedback::where('user_id',$id)->count();

        $appoinments = Appoinment::where('patient_id',$patient_id)->orderBy('id','asc')->get();
        return view('patient.dashboard', compact('patient','appoinments','my_total_appoinments','my_total_feedbacks'));
    }

    public function profile(){
        $id = Auth::user()->id;
        $patient = User::where('id',$id)->first();
        return view('patient.profile.index', compact('patient'));
    }


    public function profile_update(Request $request,$id){


        $patient = Patient::where('id',$id)->get()->first();

        if($request->avatar != ''){
            $this->validate($request,[
                'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
                [
                    'avatar.image'=>'Provide a valid image.',
                ]
            );

            $path = public_path().'/patients/avatar/';

            //code for remove old file
            if($patient->avatar != ''  && $patient->avatar != null){
                $file_old = $path.$patient->avatar;
                unlink($file_old);
            }

            //upload new file
            $image = $request->avatar;
            $imagename = time().'.'.$image->extension();
            $image->move($path, $imagename);

            //for update in table
//            $doctor->update(['avatar' => $imagename]);

//            $doctor['avatar'] = $request->imagename;
            $patient->update(['avatar' => $imagename]);
        }

        $patient['father_name'] = $request->father_name;
        $patient['mobile_no'] = $request->mobile_no;
        $patient['date_of_birth'] = $request->date_of_birth;
        $patient['age'] = $request->age;
        $patient['gender'] = $request->gender;
        $patient['occupation'] = $request->occupation;
        $patient['maritial_status'] = $request->maritial_status;
        $patient['religion'] = $request->religion;
        $patient['blood_group'] = $request->blood_group;
        $patient['address'] = $request->address;

        $patient->update();

        return redirect()->back()->with('message', 'Patient information Updated Successfully.');

    }


    public function my_prescription(){
        $id = Auth::user()->id;
        $patient = User::where('id',$id)->first();
        $pid = $patient->patient->id;
        $my_prescriptions = Prescription::where('patient_id',$pid)->get();

        return view('patient.prescription.index', compact('my_prescriptions','patient'));


    }


}
