<?php

namespace App\Http\Controllers;

use App\Model\Appoinment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppoinmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $appoinment = new Appoinment;

        $appoinment->doctor_id = $request->doctor_id;
        $appoinment->patient_id = Auth::user()->patient->id;
        $appoinment->date = $request->appoinment_date;
        $appoinment->time = $request->appoinment_time;
        $appoinment->problem = $request->problem;
        $appoinment->payment_id = $request->payment_id =='bkash' ?'1':'2';
        $appoinment->transactionid = $request->transactionid;

        $appoinment->save();
        return back()->with('message','You appointed successfully.Please wait for doctor confirmation');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Appoinment  $appoinment
     * @return \Illuminate\Http\Response
     */
    public function show(Appoinment $appoinment)
    {
        return view('appoinment.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Appoinment  $appoinment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appoinment $appoinment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Appoinment  $appoinment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appoinment $appoinment)
    {



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Appoinment  $appoinment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appoinment $appoinment)
    {
        //
    }
}
