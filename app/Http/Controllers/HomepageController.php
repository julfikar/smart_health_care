<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Doctor;
use App\Model\Feedback;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class HomepageController extends Controller
{


    public function index(){
        $role = Role::where('slug','doctor')->first();
        $doctors = User::where('role_id', $role->id)->limit(8)->get();
        $departments = Department::orderBy('name','asc')->limit(8)->get();

        return view('frontend.pages.index', compact('departments','doctors'));
    }

    public function all_doctors(){
        $role = Role::where('slug','doctor')->first();
        $doctors = User::where('role_id', $role->id)->paginate(8);
        $departments = Department::all();
        return view('frontend.pages.doctors.all_doctors',compact('departments','doctors'));
    }

    public function doctor_details($slug=null)
    {
        $role = Role::where('slug', 'doctor')->first();
        $conditions = array(
            'role_id' => $role->id,
            'slug' => $slug
        );
        $doctor = User::where($conditions)->first();

        $feedbacks = Feedback::where('doctor_id', $doctor->id)->get();


        return view('frontend.pages.doctors.doctor_details',compact('doctor','feedbacks'));
    }


    public function all_departments(){
        $role = Role::where('slug','doctor')->first();
        $doctors = User::where('role_id', $role->id)->get();
        $departments = Department::all();
        return view('frontend.pages.departments.all_departments',compact('departments','doctors'));
    }

    public function department_details($slug=null){
        $conditions = array(
            'slug'=>$slug
        );
        $department = Department::where($conditions)->first();

        return view('frontend.pages.departments.department_details',compact('department'));
    }




    public function contact(){
        return view('frontend.pages.contact');
    }

    public function about(){
        return view('frontend.pages.about');
    }

    public function search(Request $request){

        $search_name = $request->search_name;
        $search_department = $request->search_department;

        /*$results = User::orWhere('fees','like', '%'.$search_name.'%')
            ->orWhere('phone','like', '%'.$search_name.'%')
            ->orderBy('id','desc')->paginate(9);*/

        $doctors = Doctor::where('fees','like', '%'.$search_name.'%')
            ->orWhere('phone','like', '%'.$search_name.'%')
            ->orWhere('department_id', 'like', '%' . $search_department . '%')

            // searches health_checks table
            ->orWhereHas('user', function ($q) use ($search_name) {
                // searches users table
                return $q->where('name', 'like', '%' . $search_name . '%');
            })->orderBy('id', 'DESC')
            ->with('user')
            ->paginate(5);

        //     dd($results);

        return view('frontend.pages.search',compact('doctors'));
    }




}
