<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Appoinment;
use App\Model\Doctor;
use App\Model\Feedback;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index(){
        $totalDoctor = User::Where('role_id',2)->get()->count();
        $totalPatient = User::Where('role_id',3)->get()->count();
        $totalAppoinment = Appoinment::all()->count();
        $totalFeedbsck = Feedback::all()->count();

        $appoinments = Appoinment::where('is_paid','0')->orderBy('created_at','asc')->latest()->get();

        return view('admin.dashboard', compact(
            'totalDoctor',
            'totalAppoinment',
            'totalPatient',
            'totalFeedbsck',
            'appoinments'
        ));
    }

    public function all_doctors(){

        $doctors = Doctor::orderBy('user_id','desc')->latest()->get();
        return view('admin.pages.doctors.all_doctors',compact('doctors'));
    }

    public function doctor_details($id=null){
        $doctor = User::find($id);
        return view('admin.pages.doctors.doctor_details',compact('doctor'));
    }

    public function doctor_confirm($id=null){

        $doctor = Doctor::find($id);
        if($doctor->status == 0){
            $doctor->status = 1;
        }else{
            $doctor->status = 0;
        }
        $doctor->save();

        Session()->flash('message', 'Doctor has been updated successfully');
        return redirect()->back();
    }

    public function delete_appoinments($id){

        $appoinment = Appoinment::find($id);

        $appoinment->delete();

        session()->flash('message', 'Appoinment Has been Deleted Successfully');
        return back();
    }



    public function confirm_paid($id=null){

        $appoinment = Appoinment::find($id);
        if($appoinment->is_paid == 0){
        $appoinment->is_paid = 1;
        }else{
            $appoinment->is_paid = 0;
        }
        $appoinment->save();

        Session()->flash('message', 'Appoinment payment successfully');
        return redirect()->back();
    }

    public function all_appoinments(){

        $appoinments = Appoinment::orderBy('id','desc')->get();
        return view('admin.pages.appoinments.appoinment',compact('appoinments'));
    }

    public function appoinment_details($id=null){
        $doctor = User::find($id);
        return view('admin.pages.doctors.doctor_details',compact('doctor'));
    }

}
