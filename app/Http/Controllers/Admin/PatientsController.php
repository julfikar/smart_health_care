<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Patient;
use Illuminate\Http\Request;

class PatientsController extends Controller
{

    public function index(){
        $patients = Patient::orderBy('user_id','asc')->get();
        return view('admin.pages.patients.all_patients',compact('patients'));
    }

    public function patient_details($id=null){
        $patient = Patient::find($id);
        return view('admin.pages.patients.patient_details',compact('patient'));
    }

}
