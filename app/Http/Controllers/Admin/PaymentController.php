<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::all();
        return view('admin.pages.payment.index', compact('payments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = new Payment();

        $payment->name = $request->name;
        $payment->short_name = str_slug($request->name);
        $payment->type = $request->type;
        $payment->no = $request->no;

        if($request['payment_image']){

            $path = public_path().'/payments/';
            //upload new file
            $image = $request->payment_image;
            $imagename = time().'.'.$image->extension();
            $image->move($path, $imagename);

        }
        $payment->image = $imagename;

        $payment->save();
        return redirect()->back()->with('message', 'Payment added Successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $payment = Payment::find($id);
        if (!is_null($payment)) {
            return view('admin.pages.payment.edit', compact('payment'));
        } else {
            return redirect()->route('admin.payments.index');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $payment = Payment::where('id',$id)->get()->first();

        if($request->payment_image != ''){
            $this->validate($request,[
                'payment_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
                [
                    'payment_image.image'=>'Provide a valid image.',
                ]
            );

            $path = public_path().'/payments/'  ;

            //code for remove old file
            if($payment->image != ''  && $payment->image != null){
                $file_old = $path.$payment->image;
                unlink($file_old);
            }

            //upload new file
            $image = $request->payment_image;
            $imagename = time().'.'.$image->extension();
            $image->move($path, $imagename);

            $payment->update(['image' => $imagename]);
        }

        $payment['name'] = $request->name;
        $payment['short_name'] = str_slug($request->name);
        $payment['type'] = $request->type;
        $payment['no'] = $request->no;

        $payment->update();
        return redirect()->back()->with('message', 'Payment method Updated Successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = Payment::where('id',$id)->get()->first();
        $path = public_path().'/payments/';
        //code for remove old file
        if($payment->image != ''  && $payment->image != null){
            $file_old = $path.$payment->image;
            unlink($file_old);
        }

        $payment->delete();
        return redirect()->back()->with('message', 'Payment method deleted Successfully.');
    }
}
